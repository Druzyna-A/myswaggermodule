package com.javainuse.swaggertest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.awt.image.SampleModel;
import java.util.List;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
/**
 * Created by fum19_000 on 2017-03-06.
 */

@Api(value = "/sample", tags = {"article"}, description = "Articles about fishing from external resources")
@RequestMapping("/sample")
public interface SampleApi {

    @ApiOperation(httpMethod = "GET", value = "Fetch articles by title, description and/or author")
    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    ResponseEntity<List<Sample>> fetchSamples(@ApiParam(value = "Regex for searching by title") @RequestParam(required = false) String titleRegex,
                                                    @ApiParam(value = "Regex for searching by description") @RequestParam(required = false) String descriptionRegex,
                                                    @ApiParam(value = "Regex for searching by author") @RequestParam(required = false) String authorRegex);
}
