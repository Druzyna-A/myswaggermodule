package com.javainuse.swaggertest.model;

/**
 * Created by fum19_000 on 2017-03-06.
 */
public class Commands {
    public String getPoolName() {
        return poolName;
    }

    public void setPoolName(String poolName) {
        this.poolName = poolName;
    }

    private String poolName;

    public Commands(String name){
        poolName=name;
    }
}
