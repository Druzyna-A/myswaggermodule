package com.javainuse.swaggertest;
import io.swagger.annotations.ApiParam;
import java.util.Arrays;
import java.util.List;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * Created by fum19_000 on 2017-03-06.
 */
@RestController
public class SampleController implements SampleApi {

    @Override
    public ResponseEntity<List<Sample>> fetchSamples(@ApiParam(value = "Regex for searching by title") @RequestParam(required = false) String titleRegex, @ApiParam(value = "Regex for searching by description") @RequestParam(required = false) String descriptionRegex, @ApiParam(value = "Regex for searching by author") @RequestParam(required = false) String authorRegex) {
        return ResponseEntity.ok().body(Arrays.asList(new Sample[]{new Sample(), new Sample()}));
    }
}
