package com.javainuse.swaggertest.controller;

/**
 * Created by fum19_000 on 2017-03-06.
 */

import com.javainuse.swaggertest.model.Commands;
import com.javainuse.swaggertest.service.CommandsService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class CommandsController {

    @Autowired
    private CommandsService commandsService;

    @ApiOperation(value = "Fishery", nickname = "createFishery", notes = "creates a Fishery", httpMethod = "GET", response = Commands.class, responseContainer = "Fishery")
    @ApiResponses(value = {@ApiResponse(code = 400, message = "Fishery exist")
            , @ApiResponse(code = 404, message = "Fishery Exist")
            , @ApiResponse(code = 200, message = "Fishery created", responseContainer = "Fishery")})
    @RequestMapping(value = "/fishery/create"
            , produces = "application/json"
            , consumes = "application/json"
            , method = RequestMethod.GET)
    public
    @ResponseBody
    Commands createUserInFishery(@RequestBody Commands commands) {
        return this.commandsService.createPlaceForFishing(commands);
    }

    @ApiOperation(value = "Subscription", nickname = "createSubscription", notes = "creates a Subscription", httpMethod = "GET", response = Commands.class, responseContainer = "Subscription")
    @ApiResponses(value = {@ApiResponse(code = 400, message = "Email adress dont exist")
            , @ApiResponse(code = 404, message = "Email adress dont Exist")
            , @ApiResponse(code = 200, message = "Email Sended", responseContainer = "Pool")})
    @RequestMapping(value = "/pool/subscription"
            , produces = "application/json"
            , consumes = "application/json"
            , method = RequestMethod.GET)
    public
    @ResponseBody
    Commands addSubscription(@RequestBody Commands commands) {
        return this.commandsService.addSubscription(commands);
    }

    @ApiOperation(value = "Search", nickname = "SearchFishery", notes = "search for fishery", httpMethod = "GET", response = Commands.class, responseContainer = "Search")
    @ApiResponses(value = {@ApiResponse(code = 400, message = "not searched correctly")
            , @ApiResponse(code = 404, message = "Fishery not searched correctly")
            , @ApiResponse(code = 200, message = "Fishery searched", responseContainer = "Search")})
    @RequestMapping(value = "/pool/search"
            , produces = "application/json"
            , consumes = "application/json"
            , method = RequestMethod.GET)
    public
    @ResponseBody
    Commands searchForFishery(@RequestBody String text) {
        return this.commandsService.search(text);
    }

    @ApiOperation(value = "Fishery", nickname = "removeFishery", notes = "remove a Fishery", httpMethod = "DELETE", response = Commands.class, responseContainer = "Fishery")
    @ApiResponses(value = {@ApiResponse(code = 400, message = "Error Fishery don't exist")
            , @ApiResponse(code = 404, message = "Error Fishery don't exist")
            , @ApiResponse(code = 200, message = "User removed from fishery", responseContainer = "Fishery")})
    @RequestMapping(value = "/fishery/remove"
            , produces = "application/json"
            , consumes = "application/json"
            , method = RequestMethod.DELETE)
    public
    @ResponseBody
    Commands removeUserFromFishery(@RequestBody int poolId) {
        return this.commandsService.deletePlaceForFishing(poolId);
    }
}


