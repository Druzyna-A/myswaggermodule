package com.javainuse.swaggertest.service;

import com.javainuse.swaggertest.model.Commands;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by fum19_000 on 2017-03-06.
 */
public interface CommandsService {


    final static Map<Long,Commands> fishDataStore = new HashMap<Long, Commands>(){
        {
            put((long) 100,new Commands("Mazury"));
        }
    };

    public Commands createPlaceForFishing(Commands poolId);
    public Commands deletePlaceForFishing(long poolId);
    public Commands addSubscription(Commands commands);
    public Commands deleteSubscription(Commands commands);
    public Commands search(String text);

    Commands getUserById(int userId);
}
