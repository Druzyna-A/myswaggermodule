package com.javainuse.swaggertest.service;

import com.javainuse.swaggertest.model.Commands;
import org.springframework.stereotype.Service;

/**
 * Created by fum19_000 on 2017-03-07.
 */

@Service("commandsService")
public class CommandsServiceImpl implements CommandsService{

    @Override
    public Commands createPlaceForFishing(Commands poolId) {
        return null;
    }

    @Override
    public Commands deletePlaceForFishing(long poolId) {
        return null;
    }

    @Override
    public Commands addSubscription(Commands commands) {
        return null;
    }

    @Override
    public Commands deleteSubscription(Commands commands) {
        return null;
    }

    @Override
    public Commands search(String text) {
        return null;
    }

    @Override
    public Commands getUserById(int userId) {
        return null;
    }
}
